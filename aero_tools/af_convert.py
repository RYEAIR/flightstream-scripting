# -*- coding: utf-8 -*-
"""
script to convert nasa style airfoil information into XFLR *.dat data type
Created on Mon Sep  9 17:29:31 2019

@author: Julius Quitter
"""

import pandas as pd


def __init__():
    wd = "C:/OpenVSP-3.18.0-win32/airfoil/"
    # airfoil filename
    airfoil_in = "C:/OpenVSP-3.18.0-win32/airfoil/jw-1_airfoils.csv"
    af_list = ["JWR", "JWJ", "JWT", "JTR", "JTT", "JFR", "JFT"]
    NASA2Xflr(wd,airfoil_in,af_list)


def NASA2Xflr(wd, af_data, af_list):
     """ converts NASA airfoil data into Xflr compatible *.dat format

    wd: working directory containing data file

    af_data: airfoil csv file to merge
        - must contain one column containing x/c coordinates, named "xc"
        - must contain upper and lower surface data points named 
        <airfoil_name>_up and <airfoil_name>_lo
    
        af_list: list of airfoils (same as <airfoil_name> headers)
    """
    af_data = pd.read_csv(airfoil_in,delimiter=';')
    
    xc = list(af_data['xc'])
    xc.reverse()
    del xc[-1]
    xc.extend(list(af_data['xc']))
    
    af_coords = []
    
    for airfoil in af_list:
        af_tmp = list(af_data[str(airfoil+"_up")])
        af_tmp.reverse()
        del af_tmp[-1]
        af_tmp.extend(list(af_data[str(airfoil+"_lo")]))
        
        af_coords.append(af_tmp)
        
    # write airfoil to file
    for idx,airfoil in enumerate(af_list):
        with open(str(wd+airfoil+".dat"), 'w') as of:
            of.write(airfoil+"\n")
            for idx2,coord in enumerate(xc):
                of.write(str(coord)+"  "+str(af_coords[idx][idx2])+"\n")