# -*- coding: utf-8 -*-
"""
Script to load VSP Aero csv results data and 
convert to meaningful, excel readable csv sheet

Created on Sun Sep  1 15:51:40 2019

@author: Julius Q
"""

import os
import pandas as pd
from io import StringIO
import six

    
def import_vspAero(ifname, ofname):
    with open(ifname, 'r') as f:
        cont = f.read()
        cont = cont.splitlines()
        
        # find lines starting with 'Results_Name' to split file into sections
        split_l = [idx for idx, l in enumerate(cont) if l.startswith('Results_Name')]


        
        # assumes: other results ALWAYS follow VSPAERO_History
        # no protection against possible overflow
        aero_data_s = []
        for idx, l in enumerate(split_l):
            if cont[l].startswith('Results_Name,VSPAERO_History'):
                aero_data_s.append(cont[l:split_l[idx+1]])

        
        aero_data_df = pd.DataFrame()
        for dataSet in aero_data_s: 
            # forget the 'Results_XXX' lines
            dataSet = [str for str in dataSet if not str.startswith('Results')]
            
            dataSet = [[str.split(',')[0], str.split(',')[-1]] for str in dataSet]
            
            dataSet = list(map(list, six.moves.zip_longest(*dataSet, fillvalue='-')))
            
            dataSet = [','.join(str) for str in dataSet]
            
            dataSet = '\n'.join(dataSet)
            
            dataSet = pd.read_csv(StringIO(dataSet))
            

            aero_data_df = aero_data_df.append(dataSet)
        
        with open(ofname, 'w') as of:
            aero_data_df.to_csv(of, index=False, line_terminator="\n")


# default run statement
import_vspAero('C:\\OpenVSP-3.18.0-win32\\airfoil\\JW-1\\VSPAERO_jw-1_test_2.csv', 'C:\\OpenVSP-3.18.0-win32\\airfoil\\JW-1\\VSPAERO_jw-1_test_2_excel.csv')    