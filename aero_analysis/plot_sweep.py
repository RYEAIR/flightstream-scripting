# script to plot given sweeps from csv files

import os
import re
import csv
import pandas as pd
import plotly.express as px

from plotly.subplots import make_subplots
import plotly.graph_objects as go
import matplotlib.pyplot as plt


def merge_FS_data(wd, var_to_merge, ofname='outfile.csv', tag_to_merge=''):
    """ merges FlightStream data files of several runs into a single csv file

    wd: working directory containing data files to merge

    var_to_merge: find that variable in header of file and merge on that

    ofname: output file name

    tag_to_merge: tag to merge on <<<< FIXME: not implemented!
        give tag the file contains to merge data on
        >>> ex: ar10_bi_tclAoA-1_data.txt
        tag = "AoA" value: "-1"
    """

    os.chdir(wd)

    data_files = [name for name in os.listdir(".") if "_data.txt" in name]

    merged = {}
    header = []
    dataset = []
    for i, data in enumerate(data_files):
        with open(data, 'r') as f:
            cont = f.read()
            cont = cont.splitlines()
            cont = [l.lstrip() for l in cont]

            # find value to join on
            join_val = [l.split(" ")[-1] for l in cont if var_to_merge in l][0]

            # find lines that break header from data
            br_lines = [idx for idx, l in enumerate(cont) if 100*'-' in l]

            try:
                merged[var_to_merge].append(join_val)
                dataset.extend([line.split("\x00\x00,")[:-1] + [join_val] for line in
                                cont[br_lines[1]+1:br_lines[2]]])

            except KeyError:
                # first file
                merged.update({var_to_merge: [join_val]})
                header = cont[br_lines[0]+1].split(", ") + [var_to_merge]
                # copy data
                # split line items on \x00\x00, remove last (empty) item and add join value
                dataset = [line.split("\x00\x00,")[:-1] + [join_val] for line in
                           cont[br_lines[1]+1:br_lines[2]]]

    with open(ofname, mode='w') as outf:
        outf_writer = csv.writer(
            outf, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
        outf_writer.writerow(header)
        for l in dataset:
            outf_writer.writerow(l)


def plot_sweep(csv_file):
    """ plots sweep contained in given csv file
    """

    sweep_df = pd.read_csv(csv_file)

    sweep_df = sweep_df.sort_values(by=["Angle of attack (Deg)", "Surface"])

    sweep_df["CD"] = sweep_df.CDi + sweep_df.CDo

    plots = [px.line(sweep_df[sweep_df.Surface == "Total"],
                     x="Angle of attack (Deg)", y="CL", title="Biplane total CL")]

    plots.append(px.line(sweep_df[sweep_df.Surface == "Total"],
                         x="Angle of attack (Deg)", y="CMy", title="Biplane CMy"))

    plots.append(px.line(sweep_df[sweep_df.Surface == "Total"],
                         x="CD", y="CL", title="Biplane drag polar"))

    for plot in plots:
        plot.show()


def comp_sweeps(csv_FS, csv_CFD):
    """ compare FlightStream to CFD sweeps
    csv_FS: (list of) FlightStream csv files
    csv_CFD: (list of) CFD csv files
    TODO: implement lists
    """

    sweep_FS_df = pd.read_csv(csv_FS)

    sweep_FS_df = sweep_FS_df.sort_values(
        by=["Angle of attack (Deg)", "Surface"])

    sweep_FS_df["CD"] = sweep_FS_df.CDi + sweep_FS_df.CDo

    sweep_FS_df = sweep_FS_df[sweep_FS_df.Surface == "Total"]

    sweep_CFD_df = pd.read_csv(csv_CFD)

    # matplotlib

    ax1 = plt.subplot(211)
    plt.plot(sweep_FS_df["Angle of attack (Deg)"], sweep_FS_df["CL"])
    plt.plot(sweep_CFD_df["alpha_deg"], sweep_CFD_df["Cl"])
    ax1.grid(True)
    ax1.set_ylabel("CL")
    ax1.set_xlabel("AoA")

    ax2 = plt.subplot(212)
    plt.plot(sweep_FS_df["Angle of attack (Deg)"], sweep_FS_df["CMy"])
    plt.plot(sweep_CFD_df["alpha_deg"], sweep_CFD_df["CM"])
    ax2.grid(True)
    ax2.set_ylabel("CM")
    ax2.set_xlabel("AoA")

    plt.show()
    return

    """ this is plotly """
    # figs = [go.Figure(), go.Figure()]

    # figs[0].add_trace(go.Scatter(
    #     x=sweep_FS_df["Angle of attack (Deg)"], y=sweep_FS_df["CL"], name="FlightStream"))
    # figs[0].add_trace(go.Scatter(x=sweep_CFD_df["alpha_deg"],
    #                              y=sweep_CFD_df["Cl"], name="CFD"))
    # # figs[0].show()
    # figs[1].add_trace(go.Scatter(
    #     x=sweep_FS_df["Angle of attack (Deg)"], y=sweep_FS_df["CL"], name="FlightStream"))
    # figs[1].add_trace(go.Scatter(x=sweep_CFD_df["alpha_deg"],
    #                              y=sweep_CFD_df["Cl"], name="CFD"))

    # fig_main = make_subplots(rows=len(figs), cols=1)

    # for idx, fig in enumerate(figs):
    #     fig_main.append_trace(fig, row=idx+1, col=1)


def get_val_from_fname(fname, tag):
    """ gets value from data filename
FIXME: produces error
    """
    # return {tag: re.search(r"*tag[\w.-]_data.txt", tag)}
