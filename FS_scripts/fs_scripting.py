# Collection of functions required for FlightStream script generation

import copy


class FSScript:
    """ FlightStream Script

    contains an FS Script including
    """

    def __init__(self, wd, base_name):
        """ FSScript constructor

        wd: working directory

        base_file: base FlightStream sim file
        """

        self.wd = wd
        self.base_name = base_name

        self.__script_items = []

    def add_script_item(self, function_name, function_script):
        self.__script_items.append({function_name: function_script})

    def clear_script_items(self):
        self.__script_items = []


# Functions for handling FlightStream scripts


def modify_FS_parameters(script, param_dict):
    """modify parameters of script (list of lines)
    according to param_dict dictionary"""

    for param, val in param_dict.items():
        l_p = [idx for idx,
               s in enumerate(script) if str(param+" ") in s]
        assert len(l_p) == 1,\
            "Key found multiple times: \""+param+"\""

        script[l_p[0]] = str(param+" "+str(val))

    return script


def find_FS_function(script, param):
    """returns complete function (incl. parameters) from FS script
    that contains given parameter
    REQUIRES: comment or empty line before and empty line after each function
    """

    l_p = [idx for idx,
           s in enumerate(script) if str(param+" ") in s][0]

    # all empty lines
    l_empty = [idx for idx, s in enumerate(script) if s == '']
    # all comment lines
    l_comment = [idx for idx, s in enumerate(script) if s.startswith("#")]

    # line number of last empty line before param
    l_empty_last = [ln for ln in l_empty if ln < l_p][-1]
    # line numer of last comment line before param
    l_comment_last = [ln for ln in l_comment if ln < l_p][-1]

    # max of lines before and first empty line after param
    l_func = [max([l_empty_last, l_comment_last])+1,
              [ln for ln in l_empty if ln > l_p][0]]

    return script[l_func[0]:l_func[1]]


def find_FS_function_by_name(script, fcn):
    """ returns complete function as list of lines if found
    REQUIRES: comment or empty line before and empty line after each function
    """
    # lines that equals function name
    l_f1 = [idx for idx, s in enumerate(script) if fcn == s]
    # all empty lines
    l_empty = [idx for idx, s in enumerate(script) if s == '']

    l_func = [l_f1[-1], [ln for ln in l_empty if ln > l_f1[-1]][0]]

    return script[l_func[0]:l_func[1]]


def get_FS_function_name(script, param):
    """ returns function name that contains given parameter
    REQUIRES: comment or empty line before and empty line after each function
    """
    return find_FS_function(script, param)[0]


def get_FS_savename(script):
    """ return first filename the script saves to

    script: script as list of lines
    """
    return script[script.index("SAVEAS")+1]


def create_FS_sweep(base_script, param_dict, param_key={}, exports=["data", "sim"],
                    wd='', base_name='', clear_solver=False, close_FS=False):
    """function that creates a sweeping FlightStream script

    base_script: base script to generate the sweep from

    param_dict:  dictionary of parameters that contains the parameter(s)
                 to sweep and the desired values in a list
                 >>> {"ANGLE_OF_ATTACK": [0,1,2,3]}
        TODO: add support for multi-sweep

    param_key: dict of parameter shorthands used for save file naming
                 >>> {"ANGLE_OF_ATTACK": "AoA"}
                 defaults to parameter names

    exports: list of things to export supported:
       "data": exports spreadsheet <default>
       {"PLOT": <plot_name>}: exports specified plots
              <plot_name> must be list of valid FS scripting plot types,
                          no error checking!
       "sim": saves simulation file <default>

    wd:  working directory <defaults to script value>

    base_name: base file name <defaults to script value>

    clear_solver: clear solution after each run
    """
    # TODO: - add support for multiple parameter sweeps at once
    #       - allow saving of plots/spreadsheet between runs

    # preliminary version: force single sweep only
    assert len(param_dict) == 1, "Multi parameter sweep currently not supported!"

    # read base script file
    with open(base_script) as f:
        script = f.read()

    # split script in list of lines
    script = script.splitlines()

    # output script list of lines
    scr_out = [""]

    # if no param key is given, default to parameter name
    def_key = {k: k for k in param_dict.keys()}
    tags = {}
    tags.update(def_key)
    tags.update(param_key)

    # get savefile path and name
    if wd == '' or base_name == '':
        def_filename = get_FS_savename(script)
        def_filename = def_filename.split("\\")
        if wd is '':
            wd = "\\".join(def_filename[:-1])
            wd += "\\"
        if base_name is '':
            base_name = def_filename[-1].split(".")[:-1]
            base_name = str(base_name)

    print(wd)
    print(base_name)
    scr_out.extend(["OPEN", wd+base_name+".fsm", ""])

    # functions to sweep
    fcns_tosweep = []
    for param in param_dict.keys():
        fcns_tosweep.append(get_FS_function_name(script, param))

    # rank of settings including actions required on changes
    set_order = {"FLUID_PROPERTIES": "reinit", "INITIALIZE_SOLVER": "reinit",
                 "SOLVER_SETTINGS": "rerun", "SOLVER_ANALYSIS_OPTIONS": "analyze"}

    sweep_req = [val for param, val in set_order.items()
                 if param in fcns_tosweep]

    print("Which functions to sweep?")
    print(fcns_tosweep)
    print("What do we need to do for that?")
    print(sweep_req)

    # TODO: intelligent sweep nesting

    # list that contains pre-sweep commands for each sweep
    pre_sweep = []
    # set up base script before sweep and each pre-sweep actions
    for i, req in enumerate(sweep_req):
        #pre_sweep[i] = []
        pre_sweep.append([])
        if req == "reinit":
            pre_sweep[i].append("SOLVER_UNINITIALIZE")
            pre_sweep[i].append("")
            # TODO: Verify if required PRIOR to solve run
            if fcns_tosweep[i] != "FLUID_PROPERTIES":
                scr_out.extend(find_FS_function_by_name(
                    script, "FLUID_PROPERTIES"))
                scr_out.append("")
            if fcns_tosweep[i] != "INITIALIZE_SOLVER":
                pre_sweep[i].append(find_FS_function_by_name(
                    script, "INITIALIZE_SOLVER"))
                pre_sweep[i].append("")
        elif req == "rerun":
            scr_out.extend(["SOLVER_UNINITIALIZE", ""])
            scr_out.extend(find_FS_function_by_name(
                script, "FLUID_PROPERTIES"))
            scr_out.append("")
            scr_out.extend(find_FS_function_by_name(
                script, "INITIALIZE_SOLVER"))
            scr_out.append("")
            if clear_solver:
                pre_sweep[i].append("SOLVER_CLEAR")
                pre_sweep[i].append("")
            # TODO: Verify that this does not automatically clear solotion
            pre_sweep[i].extend(
                (find_FS_function_by_name(script, "SET_SOLVER")))
            pre_sweep[i].append("")
            # this should not be required as it is part of the sweep itself
            # pre_sweep[i].extend((find_FS_function_by_name(script, "SOLVER_SETTINGS")))
        elif req == "analyze":
            scr_out.append("SOLVER_UNINITIALIZE")
            scr_out.append("")
            scr_out.extend(find_FS_function_by_name(
                script, "FLUID_PROPERTIES"))
            scr_out.append("")
            scr_out.append(find_FS_function_by_name(
                script, "INITIALIZE_SOLVER"))
            scr_out.append("")
            scr_out.extend(find_FS_function_by_name(script, "SET_SOLVER"))
            scr_out.append("")
            scr_out.extend(
                (find_FS_function_by_name(script, "SOLVER_SETTINGS")))
        else:
            assert false, "Unknown sweep requirement: "+str(req)

    post_sweep = []
    for param, sweep in param_dict.items():
        for val in sweep:
            post_sweep.append(gen_FS_exports(
                wd, base_name, tags[param]+str(val), exports))

    sweep = gen_FS_sweep_fcn(script, param_dict)

    for i, sw in enumerate(sweep):
        # currently only supports single parameter
        scr_out.extend(pre_sweep[0])
        scr_out.extend(sweep[i])
        scr_out.extend(["", "START_SOLVER", ""])
        # TODO: check if start solver is correct here!
        scr_out.extend(post_sweep[i])

    if close_FS:
        scr_out.append("CLOSE_FLIGHTSTREAM")

    return "\n".join(scr_out)


def gen_FS_sweep_fcn(script, param_dict):
    """ returns list of functions containing selected parameter sweep

    script: script that contains function prototype as list of lines

    param_dict:  dictionary of parameters that contains the parameter(s)
                 to sweep and the desired values in a list
                 >>> {"ANGLE_OF_ATTACK": [0,1,2,3]}
        TODO: add support for multi-sweep
    """

    assert len(param_dict) == 1, "Currently only single parameter sweep supported!"

    # create list of default functions to use
    func_def = []
    for param in param_dict.keys():
        func_def.append(find_FS_function(script, str(param)))

    sweep_fcns = []

    # prepared for multi parameters, currently only single parameter supported!
    for param, sweep in param_dict.items():
        for val in sweep:
            sweep_fcns.append(
                copy.deepcopy(modify_FS_parameters(func_def[0], {param: val})))

    return sweep_fcns


def gen_FS_exports(wd, base_name, tag, exports):
    """ generates FS script export part (list of lines)

    wd:  working directory

    base_name: base file name

    tag: name tag (used in sweeps)

    exports: list of things to export supported:
       "data": exports spreadsheet
       {"PLOT": <plot_name>}: exports specified plots
              <plot_name> must be list of valid FS scripting plot types,
                          no error checking!
       "sim": saves simulation file
    """

    ret_str = [""]

    assert isinstance(
        exports, list), "This argument should be a list: "+str(exports)

    for exp in exports:
        if isinstance(exp, dict):
            plots = exp.get("PLOT")
            assert (plots != None) \
                and isinstance(plots, list),\
                "Unsupported export inside "+str(exp)+" : "+str(plots)
            for plot in plots:
                ret_str.append("")
                ret_str.append("SET_PLOT_TYPE")
                ret_str.append(plot)
                ret_str.append("")
                ret_str.append("SAVE_PLOT_TO_FILE")
                ret_str.append(wd+base_name+"_"+tag+"_"+plot+".txt")
                ret_str.append("")
        elif exp == "data":
            ret_str.append("")
            ret_str.append("EXPORT_SOLVER_ANALYSIS_SPREADSHEET")
            ret_str.append(wd+base_name+tag+"_data.txt")
            ret_str.append("")
        elif exp == "sim":
            ret_str.append("")
            ret_str.append("SAVEAS")
            ret_str.append(wd+base_name+tag+".fsm")
            ret_str.append("")
        else:
            assert False, "Unsupported export: " + \
                str(exp)+" of type: " + str(type(exp))
    return ret_str
