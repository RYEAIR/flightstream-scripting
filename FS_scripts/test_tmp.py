import unittest

from fs_scripting import *


class TestScriptGenerator(unittest.TestCase):

    def test_sweep_generation(self):
        param_dict = {"ANGLE_OF_ATTACK": [0, 1]}

        script = ['#************************************************************************',
                  '#********* Set the solver settings **************************************',
                  '#************************************************************************',
                  '#',
                  '',
                  'SET_SOLVER',
                  'STEADY',
                  '',
                  'SOLVER_SETTINGS',
                  'ANGLE_OF_ATTACK 3',
                  'SIDESLIP_ANGLE 0',
                  'FREESTREAM_VELOCITY 50',
                  'ITERATIONS 300',
                  'CONVERGENCE_LIMIT 0.00001',
                  'FORCED_RUN DISABLE',
                  'COMPRESSIBILITY KARMAN-TSIEN',
                  'REFERENCE_VELOCITY 50',
                  'REFERENCE_AREA 5',
                  'REFERENCE_LENGTH 1',
                  'PROCESSORS 12',
                  'WAKE_SIZE 100',
                  '', ]

        pos_result = [['SOLVER_SETTINGS',
                       'ANGLE_OF_ATTACK 0',
                       'SIDESLIP_ANGLE 0',
                       'FREESTREAM_VELOCITY 50',
                       'ITERATIONS 300',
                       'CONVERGENCE_LIMIT 0.00001',
                       'FORCED_RUN DISABLE',
                       'COMPRESSIBILITY KARMAN-TSIEN',
                       'REFERENCE_VELOCITY 50',
                       'REFERENCE_AREA 5',
                       'REFERENCE_LENGTH 1',
                       'PROCESSORS 12',
                       'WAKE_SIZE 100'],
                      ['SOLVER_SETTINGS',
                       'ANGLE_OF_ATTACK 1',
                       'SIDESLIP_ANGLE 0',
                       'FREESTREAM_VELOCITY 50',
                       'ITERATIONS 300',
                       'CONVERGENCE_LIMIT 0.00001',
                       'FORCED_RUN DISABLE',
                       'COMPRESSIBILITY KARMAN-TSIEN',
                       'REFERENCE_VELOCITY 50',
                       'REFERENCE_AREA 5',
                       'REFERENCE_LENGTH 1',
                       'PROCESSORS 12',
                       'WAKE_SIZE 100']]

        sweep_fcns = gen_FS_sweep_fcn(script, param_dict)
        self.assertEqual(sweep_fcns, pos_result)


# run tests if script run alone
if __name__ == '__main__':
    unittest.main()
