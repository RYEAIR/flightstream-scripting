# this script generates a FlightStream script and runs the application
#
# planned features:
# [ ] modify solver settings
# [ ] generate custom sweep script with auto saving
# [ ] automatically plot results


# SCRIPT >>>
from fs_scripting import *

# sweep generator test script
base_file = "base_bip.txt"

# sweep_param = {"ANGLE_OF_ATTACK": list(range(-3, 20, 1))}
# param_key = {"ANGLE_OF_ATTACK": "AoA"}

sweep_param = {"WAKE_SIZE": list(range(25, 200, 25))}
param_key = {"WAKE_SIZE": "WkRef"}


exports = ["data", "sim", {"PLOT": ["RESIDUALS", "LOADS"]}]

wd = "C:\\FlightStream_projects\\FS_validation\\simple_wings\\FlightStream\\AR10_biplane\\biplane_cam_tail_wakeSweep\\"
base_name = "4414_biplane_tail"

sweep_scr = create_FS_sweep(
    base_file, sweep_param, param_key, exports, wd, base_name)

with open('bip_wkref_sweep.txt', 'w') as f:
    f.write(sweep_scr)
